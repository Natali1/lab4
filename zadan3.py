from random import random
n = 20
M = []
for i in range(n):
    M.append(int(random()*100))
M.sort()
print(M)

while True:
    try:
        number = int(input("Введите число из этого массива:"))
    except ValueError:
        print("Ошибка")
    else:
        break

left = 0
right = n-1
while left <= right:
    mid = (left + right) // 2
    if number < M[mid]:
        right = mid - 1
    elif number > M[mid]:
        left = mid + 1
    else:
        print("id =", mid)
        break
else:
    print("Такого числа нет в массиве")
