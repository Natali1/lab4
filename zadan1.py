while True:
    try:
        N = int(input("Введите число: "))
    except ValueError:
        print("Ошибка")
        continue
    if N < 0:
        print("Ошибка")
        continue
    else:
        break
F = 1
summa = 1
for i in range(1, N + 1):
    F *= i
    summa += F
print(summa)
